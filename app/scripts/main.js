$(document).ready(function() {

  $('.home-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: '<button type="button" class="slick-prev"><img src="../images/icon_left.png"></button>',
    nextArrow: '<button type="button" class="slick-next"><img src="../images/icon_right.png"></button>',
    dots: true,
    infinite: true,
    speed: 300
  });

  $('.home-reviews-slider').slick({

    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: '<button type="button" class="slick-prev"><img src="../images/icon_left.png"></button>',
    nextArrow: '<button type="button" class="slick-next"><img src="../images/icon_right.png"></button>',
    dots: true,
    infinite: true,
    speed: 300
  });

  $('.catalog-nav__toggle').hover(function() {
    $(this).children().next('.catalog-nav__sub-nav').slideToggle();
  });

  $('.filter-block__header').click(function() {
    $(this).next('.filter-block__body').slideToggle();
  });

  var sync1 = $('#sync1');
  var sync2 = $('#sync2');

  sync1.owlCarousel({
    singleItem: true,
    slideSpeed: 1000,
    navigation: false,
    pagination: false,
    afterAction: syncPosition,
    responsiveRefreshRate: 200,
  });

  sync2.owlCarousel({
    items: 4,
    itemsDesktop: [1199, 4],
    itemsDesktopSmall: [979, 4],
    itemsTablet: [768, 3],
    itemsMobile: [479, 3],
    pagination: false,
    navigation: false,
    responsiveRefreshRate: 0,
    navigationText: [
      '<img src=\'images/left.png\'>',
      '<img src=\'images/right.png\'>'
    ],
    afterInit: function(el) {
      el.find('.owl-item').eq(0).addClass('synced');
    }
  });

  function syncPosition(el) {
    var current = this.currentItem;
    $('#sync2')
      .find('.owl-item')
      .removeClass('synced')
      .eq(current)
      .addClass('synced')
  }

  $('#sync2').on('click', '.owl-item', function(e) {
    e.preventDefault();
    var number = $(this).data('owlItem');
    sync1.trigger('owl.goTo', number);
  });

  function center(number) {
    var sync2visible = sync2.data('owlCarousel').owl.visibleItems;
    var num = number;
    var found = false;
    for (var i in sync2visible) {
      if (num === sync2visible[i]) {
        var found = true;
      }
    }

    if (found === false) {
      if (num > sync2visible[sync2visible.length - 1]) {
        sync2.trigger('owl.goTo', num - sync2visible.length + 2)
      } else {
        if (num - 1 === -1) {
          num = 0;
        }
        sync2.trigger('owl.goTo', num);
      }
    } else if (num === sync2visible[sync2visible.length - 1]) {
      sync2.trigger('owl.goTo', sync2visible[1])
    } else if (num === sync2visible[0]) {
      sync2.trigger('owl.goTo', num - 1)
    }

  }

});

(function($) {
  $.fn.shorten = function(settings) {

    var config = {
      showChars: 100,
      ellipsesText: '...',
      moreText: 'more',
      lessText: 'less'
    };

    if (settings) {
      $.extend(config, settings);
    }

    $(document).off('click', '.morelink');

    $(document).on({
      click: function() {

        var $this = $(this);
        if ($this.hasClass('less')) {
          $this.removeClass('less');
          $this.html(config.moreText);
        } else {
          $this.addClass('less');
          $this.html(config.lessText);
        }
        $this.parent().prev().slideToggle('slow');
        $this.prev().slideToggle('slow');
        return false;
      }
    }, '.morelink');

    return this.each(function() {
      var $this = $(this);
      if ($this.hasClass('shortened')) return;

      $this.addClass('shortened');
      var content = $this.html();
      if (content.length > config.showChars) {
        var c = content.substr(0, config.showChars);
        var h = content.substr(config.showChars, content.length - config.showChars);
        var html = c + '<span class="moreellipses">' + config.ellipsesText + ' </span><span class="morecontent"><span>' + h + '</span> <a href="#" class="morelink">' + config.moreText + '</a></span>';
        $this.html(html);
        $('.morecontent span').hide();
      }
    });

  };

})(jQuery);

/*!
 * @preserve
 *
 * Readmore.js jQuery plugin
 * Author: @jed_foster
 * Project home: http://jedfoster.github.io/Readmore.js
 * Licensed under the MIT license
 *
 * Debounce function from http://davidwalsh.name/javascript-debounce-function
 */
! function(t) { 'function' == typeof define && define.amd ? define(['jquery'], t) : 'object' == typeof exports ? module.exports = t(require('jquery')) : t(jQuery) }(function(t) {
  'use strict';

  function e(t, e, i) {
    var a;
    return function() {
      var n = this,
        o = arguments,
        r = function() { a = null, i || t.apply(n, o) },
        s = i && !a;
      clearTimeout(a), a = setTimeout(r, e), s && t.apply(n, o)
    }
  }

  function i(t) { var e = ++h; return String(null == t ? 'rmjs-' : t) + e }

  function a(t) {
    var e = t.clone().css({ height: 'auto', width: t.width(), maxHeight: 'none', overflow: 'hidden' }).insertAfter(t),
      i = e.outerHeight(),
      a = parseInt(e.css({ maxHeight: '' }).css('max-height').replace(/[^-\d\.]/g, ''), 10),
      n = t.data('defaultHeight');
    e.remove();
    var o = a || t.data('collapsedHeight') || n;
    t.data({ expandedHeight: i, maxHeight: a, collapsedHeight: o }).css({ maxHeight: 'none' })
  }

  function n(t) {
    if (!d[t.selector]) {
      var e = ' ';
      t.embedCSS && '' !== t.blockCSS && (e += t.selector + ' + [data-readmore-toggle], ' + t.selector + '[data-readmore]{' + t.blockCSS + '}'), e += t.selector + '[data-readmore]{transition: height ' + t.speed + 'ms;overflow: hidden;}',
        function(t, e) {
          var i = t.createElement('style');
          i.type = 'text/css', i.styleSheet ? i.styleSheet.cssText = e : i.appendChild(t.createTextNode(e)), t.getElementsByTagName('head')[0].appendChild(i)
        }(document, e), d[t.selector] = !0
    }
  }

  function o(e, i) { this.element = e, this.options = t.extend({}, s, i), n(this.options), this._defaults = s, this._name = r, this.init(), window.addEventListener ? (window.addEventListener('load', l), window.addEventListener('resize', l)) : (window.attachEvent('load', l), window.attachEvent('resize', l)) }
  var r = 'readmore',
    s = { speed: 100, collapsedHeight: 200, heightMargin: 16, moreLink: '<a href="#">Read More</a>', lessLink: '<a href="#">Close</a>', embedCSS: !0, blockCSS: 'display: block; width: 100%;', startOpen: !1, beforeToggle: function() {}, afterToggle: function() {} },
    d = {},
    h = 0,
    l = e(function() {
      t('[data-readmore]').each(function() {
        var e = t(this),
          i = 'true' === e.attr('aria-expanded');
        a(e), e.css({ height: e.data(i ? 'expandedHeight' : 'collapsedHeight') })
      })
    }, 100);
  o.prototype = {
    init: function() {
      var e = t(this.element);
      e.data({ defaultHeight: this.options.collapsedHeight, heightMargin: this.options.heightMargin }), a(e);
      var n = e.data('collapsedHeight'),
        o = e.data('heightMargin');
      if (e.outerHeight(!0) <= n + o) return !0;
      var r = e.attr('id') || i(),
        s = this.options.startOpen ? this.options.lessLink : this.options.moreLink;
      e.attr({ 'data-readmore': '', 'aria-expanded': this.options.startOpen, id: r }), e.after(t(s).on('click', function(t) { return function(i) { t.toggle(this, e[0], i) } }(this)).attr({ 'data-readmore-toggle': '', 'aria-controls': r })), this.options.startOpen || e.css({ height: n })
    },
    toggle: function(e, i, a) {
      a && a.preventDefault(), e || (e = t('[aria-controls="' + _this.element.id + '"]')[0]), i || (i = _this.element);
      var n = t(i),
        o = '',
        r = '',
        s = !1,
        d = n.data('collapsedHeight');
      n.height() <= d ? (o = n.data('expandedHeight') + 'px', r = 'lessLink', s = !0) : (o = d, r = 'moreLink'), this.options.beforeToggle(e, n, !s), n.css({ height: o }), n.on('transitionend', function(i) { return function() { i.options.afterToggle(e, n, s), t(this).attr({ 'aria-expanded': s }).off('transitionend') } }(this)), t(e).replaceWith(t(this.options[r]).on('click', function(t) { return function(e) { t.toggle(this, i, e) } }(this)).attr({ 'data-readmore-toggle': '', 'aria-controls': n.attr('id') }))
    },
    destroy: function() {
      t(this.element).each(function() {
        var e = t(this);
        e.attr({ 'data-readmore': null, 'aria-expanded': null }).css({ maxHeight: '', height: '' }).next('[data-readmore-toggle]').remove(), e.removeData()
      })
    }
  }, t.fn.readmore = function(e) {
    var i = arguments,
      a = this.selector;
    return e = e || {}, 'object' == typeof e ? this.each(function() {
      if (t.data(this, 'plugin_' + r)) {
        var i = t.data(this, 'plugin_' + r);
        i.destroy.apply(i)
      }
      e.selector = a, t.data(this, 'plugin_' + r, new o(this, e))
    }) : 'string' == typeof e && '_' !== e[0] && 'init' !== e ? this.each(function() {
      var a = t.data(this, 'plugin_' + r);
      a instanceof o && 'function' == typeof a[e] && a[e].apply(a, Array.prototype.slice.call(i, 1))
    }) : void 0
  }
});